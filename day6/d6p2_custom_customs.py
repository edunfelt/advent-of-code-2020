def extract_forms(batch):
    """ Extract form data from list of inputs"""
    forms = []
    form_data = []
    for entry in batch:
        form_data = entry.replace("\n", " ").split(" ")
        forms.append(form_data)
    return forms


def count_questions(form):
    """ Count the number of questions answered all members of a group of people """
    questions = "abcdefghijklmnopqrstuvwxyz"
    answers = ""
    people = len(form)
    count = 0
    if people == 1:
        return len(form[0])
    for entry in form:
        answers += entry
    for char in questions:
        if answers.count(char) == people:
            count += 1
    return count


def get_total_questions(batch):
    """ Get the total number of questions answered by every member of each group """
    data = extract_forms(batch)
    total = 0
    for form in data:
        print(count_questions(form))
        total += count_questions(form)
    return total


def main():
    with open("day6/d6_data", "r") as f:
        customs_forms = f.read()
    customs_forms = customs_forms.split("\n\n")
    
    print(get_total_questions(customs_forms))


if __name__ == "__main__":
    main()
