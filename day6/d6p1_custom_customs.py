def extract_forms(batch):
    """ Extract form data from list of inputs"""
    forms = []
    form_data = []
    for entry in batch:
        form_data = entry.replace("\n", " ").split(" ")
        forms.append(form_data)
    return forms


def count_questions(form):
    """ Count the number of questions answered by a group of people """
    questions = []
    for entry in form:
        for char in entry:
            if char not in questions:
                questions.append(char)
    return len(questions)


def get_total_questions(batch):
    """ Get the total number of answered questions from list of inputs """
    data = extract_forms(batch)
    total = 0
    for form in data:
        total += count_questions(form)
    return total


def main():
    with open("day6/d6_data", "r") as f:
        customs_forms = f.read()
    customs_forms = customs_forms.split("\n\n")

    print(get_total_questions(customs_forms))


if __name__ == "__main__":
    main()
