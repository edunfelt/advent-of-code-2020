class bag_graph():
    """
    Create a graph of the bags mentioned in the rules.
    Each key in 'bags' represents a bag, and the corresponding value is a list of bags it containes.
    """
    def __init__(self, rules):
        self.bags = {}
        rules = [rule.split() for rule in rules]
        for rule in rules:
            bag_names = [rule[0] + " " + rule[1]]
            bag_names = []
            for i in range(len(rule)):
                if rule[i].isdigit():
                    bag_names.append(rule[i+1] + " " + rule[i+2])
            self.bags[rule[0] + " " + rule[1]] = bag_names


    def contained_in(self, bag):
        """ Traverse the tree and find a list of bags which contain the bag 'bag' """
        valid_bags = []
        for bag_node in self.bags:
            if bag in self.bags.get(bag_node) and bag_node not in valid_bags:
                valid_bags.append(bag_node)
                valid_bags.extend(self.contained_in(bag_node))
        total_valid = []
        for valid_bag in valid_bags:
            if valid_bag not in total_valid:
                total_valid.append(valid_bag)
        return total_valid


def main():
    with open("day7/d7_data", "r") as f:
        rule_list = []
        for line in f:
            rule_list.append(line.strip("\n"))

    rule_data = bag_graph(rule_list)
    print(len(rule_data.contained_in("shiny gold")))


if __name__ == "__main__":
    main()
