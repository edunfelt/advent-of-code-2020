class bag_graph():
    """
    Create a graph of the bags mentioned in the rules.
    Each key in 'bags' represents a bag, and the corresponding value is a list of bags it containes.
    Each key in 'bag_content' represents a bag, and the value is a dictionary in which a key represents
    a bag and a value represents the number of such bags that are contained in the outer bag.
    """
    def __init__(self, rules):
        self.bags = {}
        self.bag_content = {}
        rules = [rule.split() for rule in rules]
        for rule in rules:
            bag_names = [rule[0] + " " + rule[1]]
            bag_content = 0
            content = {}
            bag_names = []
            for i in range(len(rule)):
                if rule[i].isdigit():
                    content[rule[i+1] + " " + rule[i+2]] = int(rule[i])
                    bag_names.append(rule[i+1] + " " + rule[i+2])
            self.bag_content[rule[0] + " " + rule[1]] = content
            self.bags[rule[0] + " " + rule[1]] = bag_names


    def contained_in(self, bag):
        """ Traverse the tree and find a list of bags which contain the bag 'bag' """
        valid_bags = []
        for bag_node in self.bags:
            if bag in self.bags.get(bag_node) and bag_node not in valid_bags:
                valid_bags.append(bag_node)
                valid_bags.extend(self.contained_in(bag_node))
        total_valid = []
        for valid_bag in valid_bags:
            if valid_bag not in total_valid:
                total_valid.append(valid_bag)
        return total_valid


    def count_content(self, bag):
        """ Traverse the tree and recursively compute the total number of bags contained in 'bag' """
        content = 0
        contained_bags = self.bags[bag]
        visited_bags = []
        for cur_bag in contained_bags:
            number_inside = self.bag_content[bag][cur_bag]
            content += number_inside * (self.count_content(cur_bag) + 1)
        return content

def main():
    with open("day7/d7_data", "r") as f:
        rule_list = []
        for line in f:
            rule_list.append(line.strip("\n"))

    rule_data = bag_graph(rule_list)
    print(rule_data.count_content("shiny gold"))


if __name__ == "__main__":
    main()
