class machine():
    """ 
    Simulate a handheld machine with the set of instructions 'acc', 'jmp', and 'nop'.
    """
    def __init__(self, instructions):
        self.reset(instructions)
    

    def reset(self, instructions):
        """ Reset to initial state """
        self.accumulator = 0
        self.cur_pos = 0
        self.instructions = instructions
        self.status = [False for x in instructions]


    def parse_instruction(self, pos):
        """ Parse the instruction at the given position """
        instruction = self.instructions[pos].split()
        name = instruction[0]
        number = int(instruction[1])
        return (name, number)

    
    def accumulate(self, number):
        """ Execute accumulate instruction """
        self.status[self.cur_pos] = True
        self.accumulator += number
        self.cur_pos += 1

    
    def jump(self, number):
        """ Execute jump instruction """
        self.status[self.cur_pos] = True
        self.cur_pos += number

    
    def nop(self):
        """ Execute No Operation instruction """
        self.status[self.cur_pos] = True
        self.cur_pos += 1

    
    def replace_instr(self):
        """ Replace jmp with nop and vice versa at the current position """
        name, number = self.parse_instruction(self.cur_pos)
        if name == "jmp":
            self.instructions[self.cur_pos] = "nop " + str(number)
        elif name == "nop":
            self.instructions[self.cur_pos] = "jmp " + str(number)
 

    def rewrite(self, position):
        """ Rewrite the program at the given position """
        self.cur_pos = position
        self.replace_instr()
        self.reset(self.instructions)


    def is_loop(self):
        """ Simulate the program and determine whether it loops """
        while self.cur_pos == len(self.instructions) or not self.status[self.cur_pos]:
            if self.cur_pos == len(self.instructions):
                return False
            name, number = self.parse_instruction(self.cur_pos)
            if name == "acc":
                self.accumulate(number)
            elif name == "jmp":
                self.jump(number)
            elif name == "nop":
                self.nop()
        return True

    
    def find_correct(self):
        """ 
        Change one instruction at a time to find a program that won't loop.
        Return the final value of a terminating program.
        """
        for i in range(len(self.instructions)):
            name, number = self.parse_instruction(i)
            if name == "jmp" or name == "nop":
                self.rewrite(i)
                if not self.is_loop():
                    return self.accumulator
                self.rewrite(i)


def main():
    with open("day8/d8_data", "r") as f:
        boot_code = []
        for line in f:
            boot_code.append(line.strip("\n"))

    handheld = machine(boot_code)
    print(handheld.find_correct())


if __name__ == "__main__":
    main()
