class machine():
    """ 
    Simulate a handheld machine with the set of instructions 'acc', 'jmp', and 'nop'.
    """
    def __init__(self, instructions):
        self.reset(instructions)
    

    def reset(self, instructions):
        """ Reset to initial state """
        self.accumulator = 0
        self.cur_pos = 0
        self.instructions = instructions
        self.status = [False for x in instructions]


    def parse_instruction(self, pos):
        """ Parse the instruction at the given position """
        instruction = self.instructions[pos].split()
        name = instruction[0]
        number = int(instruction[1])
        return (name, number)

    
    def accumulate(self, number):
        """ Execute accumulate instruction """
        self.status[self.cur_pos] = True
        self.accumulator += number
        self.cur_pos += 1

    
    def jump(self, number):
        """ Execute jump instruction """
        self.status[self.cur_pos] = True
        self.cur_pos += number

    
    def nop(self):
        """ Execute No Operation instruction """
        self.status[self.cur_pos] = True
        self.cur_pos += 1

    
    def run_instructions(self):
        """ Simulate the program and return accumulator value before infinite loop restarts """
        while not self.status[self.cur_pos]:
            name, number = self.parse_instruction(self.cur_pos)
            if name == "acc":
                self.accumulate(number)
            elif name == "jmp":
                self.jump(number)
            elif name == "nop":
                self.nop()
        return self.accumulator


def main():
    with open("day8/d8_data", "r") as f:
        boot_code = []
        for line in f:
            boot_code.append(line.strip("\n"))

    handheld = machine(boot_code)
    print(handheld.run_instructions())


if __name__ == "__main__":
    main()
