def move(posx, posy):
    """ Move according to the given pattern """
    return posx + 3, posy + 1


def count_trees(map):
    """ Count the number of trees encountered on the trajectory """
    tree_count = 0
    posx, posy = 0, 0
    for row in map:
        posx, posy = move(posx, posy)
        if posy == len(map):
            return tree_count
        elif map[posy][posx % 31] == "#":   # wrap around where the map "line" ends
            tree_count += 1


def main():
    toboggan_map = []
    with open("day3/d3_data", "r") as f:
        for line in f:
            toboggan_map.append(line.strip("\n"))

    print(count_trees(toboggan_map))


if __name__ == "__main__":
    main()