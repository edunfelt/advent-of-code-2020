def move(posx, posy, xslope, yslope):
    """ Move according to the given pattern """
    return posx + xslope, posy + yslope


def count_trees(map, xslope, yslope):
    """ Count the number of trees encountered on the trajectory given by (xslope, yslope) """
    tree_count = 0
    posx, posy = 0, 0
    for row in map:
        posx, posy = move(posx, posy, xslope, yslope)
        if posy >= len(map):
            return tree_count
        elif map[posy][posx % 31] == "#":   # wrap around where the map "line" ends
            tree_count += 1


def multiply_all(map):
    """ Multiply all results """
    product = count_trees(map, 1, 1)
    product *= count_trees(map, 3, 1)
    product *= count_trees(map, 5, 1)
    product *= count_trees(map, 7, 1)
    product *= count_trees(map, 1, 2)
    return product


def main():
    toboggan_map = []
    with open("day3/d3_data", "r") as f:
        for line in f:
            toboggan_map.append(line.strip("\n"))

    print(multiply_all(toboggan_map))


if __name__ == "__main__":
    main()