def get_lower(seq):
    """ Get the lower half of list (assumed to be of even length) """
    return seq[:int(len(seq) / 2)]


def get_upper(seq):
    """ Get the upper half of list (assumed to be of even length) """
    return seq[int(len(seq) / 2):]


def find_row(boarding_pass):
    """ Use BSP to find the seat row """
    rows = range(128)
    for c in boarding_pass:
        if c == "F":
           rows = get_lower(rows)
        elif c == "B":
            rows = get_upper(rows)
    return rows[0]


def find_column(boarding_pass):
    """ Use BSP to find seat column """
    columns = range(8)
    for c in boarding_pass:
        if c == "R":
            columns = get_upper(columns)
        elif c == "L":
            columns = get_lower(columns)
    return columns[0]


def find_seat(boarding_pass):
    """ Find the row and column of a seat and compute its ID """
    row = find_row(boarding_pass)
    col = find_column(boarding_pass)
    return row * 8 + col


def find_max_seat(passes):
    """ Find the seat with max ID """
    max_seat = 0
    for boarding_pass in passes:
        seat_id = find_seat(boarding_pass)
        if seat_id > max_seat:
            max_seat = seat_id
    return max_seat


def main():
    with open("day5/d5_data", "r") as f:
        boarding_passes = []
        for line in f:
            boarding_passes.append(line.strip("\n"))

    print(find_max_seat(boarding_passes))


if __name__ == "__main__":
    main()
