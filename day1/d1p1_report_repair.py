def find_2020_sum_two(report):
    """ Find the two digits that sum to 2020 and multiply them """
    entries = len(report)
    for i in range(entries):
        for j in range(i + 1, entries):
            expense_sum = report[i] + report[j]
            if expense_sum == 2020:
                return report[i] * report[j]


def main():
    expense_report = []
    with open("day1/d1_data", "r") as f:
        for line in f:
            expense_report.append(int(line.strip("\n")))
    print(find_2020_sum_two(expense_report))


if __name__ == "__main__":
    main()
