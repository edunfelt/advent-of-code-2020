# Emilia's 2020 Advent of Code Solutions

These are my solutions to the Advent of Code problems 2020, written in Python. 
The project is not incredibly ambitious, as I am doing these mainly for fun - although who doesn't like some practice in problem-solving during the holidays?

## Structure
Each folder contains my solutions to the two parts to each day's problem, as well as 
the problem description, and the puzzle input.

## About AoC
From [Advent of Code about section](https://adventofcode.com/2020/about):
> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.

