def extract_passports(batch):
    """ Extract the password fields as a dictionary and store in list """
    passports = []
    passport_data = []
    for entry in batch:
        passport = {}
        passport_data = entry.replace("\n", " ").split(" ")
        for kv_pair in passport_data:
            key, value = kv_pair.split(":")
            passport[key] = value
        passports.append(passport)
    return passports


def check_valid(passport):
    """ Check if a given passport is valid """
    if len(passport) == 8 or (len(passport) == 7 and not passport.get("cid")):
        return True
    else:
        return False


def count_valid(batch):
    """ Count the number of valid passports """
    valid_count = 0
    passports = extract_passports(batch)
    for passport in passports:
        if check_valid(passport):
            valid_count += 1
    return valid_count


def main():
    with open("day4/d4_data", "r") as f:
        batch_data = f.read()
    batch_file = batch_data.split("\n\n")

    print(count_valid(batch_file))


if __name__ == "__main__":
    main()