import re


def extract_passports(batch):
    """ Extract the password fields as a dictionary and store in list """
    passports = []
    passport_data = []
    for entry in batch:
        passport = {}
        passport_data = entry.replace("\n", " ").split(" ")
        for kv_pair in passport_data:
            key, value = kv_pair.split(":")
            passport[key] = value
        passports.append(passport)
    return passports


# Checks for specific fields:
def check_byr(passport):
    if 1920 <= int(passport.get("byr")) <= 2002:
        return True
    else:
        return False


def check_iyr(passport):
    if 2010 <= int(passport.get("iyr")) <= 2020:
        return True
    else:
        return False


def check_eyr(passport):
    if 2020 <= int(passport.get("eyr")) <= 2030:
        return True
    else:
        return False


def check_hgt(passport):
    if passport.get("hgt"):
        unit = passport.get("hgt")[-2:]
        value = int(passport.get("hgt")[:-2])
        if unit == "cm" and 150 <= value <= 193:
            return True
        elif unit == "in" and 59 <= value <= 76:
            return True
    else:
        return False


def check_hcl(passport):
    value = passport.get("hcl")
    if value[0] == "#" and re.match("[a-f0-9]", value[1:]) and len(value) == 7:
        return True
    else:
        return False


def check_ecl(passport):
    if passport.get("ecl") in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
        return True
    else:
        return False


def check_pid(passport):
    value = passport.get("pid")
    if len(value) == 9 and re.match("[0-9]", value):
        return True
    else:
        return False


def check_all(passport):
    """ Check that all fields are valid according to specification (except cid) """
    if len(passport) == 8 or (len(passport) == 7 and not passport.get("cid")):
        if check_byr(passport) and check_iyr(passport) and check_eyr(passport) and check_hgt(passport) and check_hcl(passport) and check_ecl(passport) and check_pid(passport):
            return True
    else:
        return False


def count_valid(batch):
    """ Count the number of valid passports """
    valid_count = 0
    passports = extract_passports(batch)
    for passport in passports:
        if check_all(passport):
            valid_count += 1
    return valid_count


def main():
    with open("day4/d4_data", "r") as f:
        batch_data = f.read()
    batch_file = batch_data.split("\n\n")

    print(count_valid(batch_file))


if __name__ == "__main__":
    main()