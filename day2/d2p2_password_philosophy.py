import re


def check_valid(policy, letter, password):
    """ Check if a given password is valid according to the policy """
    pos1 = password[policy[0] - 1]
    pos2 = password[policy[1] - 1]
    if (pos1 == letter and pos2 != letter) or (pos1 != letter and pos2 == letter):
        return True
    else:
        return False


def extract_policy(entry):
    """ Extract the policy and password information from string """
    data = re.split("-|: | ", entry)    # split at '-', ':' and ' '
    return (int(data[0]), int(data[1])), data[2], data[3]


def count_valid(pw_list):
    """ Count the number of valid passwords in the given list """
    valid_count = 0
    for entry in pw_list:
        policy, letter, pw = extract_policy(entry)
        if check_valid(policy, letter, pw):
            valid_count += 1
    return valid_count


def main():
    passwords = []
    with open("day2/d2_data", "r") as f:
        for line in f:
            passwords.append(line.strip("\n"))

    print(count_valid(passwords))


if __name__ == "__main__":
    main()